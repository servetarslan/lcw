import LazyLoad from "vanilla-lazyload";

const mobileMenu = () => {
    $(".menu-navigation").on("click", () => {
        $(".menu-navigation").toggleClass("active");
        $("header").toggleClass("active");
        $("body").toggleClass("locked");
    })
}


const gotodown = () => {
    let btn = $(".icon-arrow");
    btn.on("click", function () {
        goto("#firstWork", "down");
    })
}

const goto = (item, type) => {
    $('html,body').animate({
        scrollTop: $(item).offset().top
    }, 'slow')
}

const headSearch = () => {

    const search = $("#headSearch");

    $(document).on("click touch", function () {
        if (search.is(":focus")) {
            $(".search-form i, .search-form span").css("opacity", "0");

        } else {
            if (search.val() === "") {
                $(".search-form i, .search-form span").css("opacity", "1");
            }
        }
    })
}


const bannerWorks = () => {
    const homeBanner = $("#homeBannerSlider");

    var swiper = new Swiper('#homeBannerSlider', {
        lazy: true,
        autoplay: true,
        effect: 'fade',
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },


        on: {
            init: function () {
                console.log('swiper initialized');
                var currentVideo = $("[data-swiper-slide-index=" + this.realIndex + "]").find("video");
                currentVideo.trigger('play');
            },
        },
    });

    var sliderVideos = $(".swiper-slide video");

    swiper.on('slideChange', function () {

        sliderVideos.each(function (index) {
            this.currentTime = 0;
        });


    });
}

const brandWorks = () => {
    var swiper = new Swiper('#brandItemLıst', {
        slidesPerView: 2,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 30,
            },
            1200: {
                slidesPerView: 6,
                spaceBetween: 40,
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
}




let ll = new LazyLoad();



$(document).ready(function () {
    mobileMenu();
    gotodown();
    bannerWorks();
    headSearch();
    brandWorks()
})