const path = require('path');

module.exports = {
    entry: "./src/assets/js/main.js",
    module: {
        rules: [{
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                }
            }
        }]
    },
    output: {
        filename: "app.js",
        path: path.join(__dirname, "./src/assets/js/")
    },
    mode: "development",
    devtool: 'source-map'
}